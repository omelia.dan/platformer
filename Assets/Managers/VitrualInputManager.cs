﻿using platformer;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace platformer
{
    public class VirtualInputManager : Singleton<VirtualInputManager>
    {
        public bool moveLeft;
        public bool moveRight;
        public bool Jump;
        public bool Attack;

    }
}

