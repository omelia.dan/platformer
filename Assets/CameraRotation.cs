﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour
{
    public CharacterControl control;

    void LateUpdate()
    {
        transform.position = control.GetPos();
    }
}
