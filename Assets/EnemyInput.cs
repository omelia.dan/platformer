﻿using platformer;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyInput : MonoBehaviour
{
    private CharacterControl characterControl;
    public LayerMask enemyLayers;
    public float detectRange = 5f;
    public float attackRange = 1f;
    public float speed;
    public float attackRate = 0.5f;
    float nextAttack = 0f;
    public float attackDistance = 0.6f;
    void Awake()
    {
        characterControl = this.gameObject.GetComponent<CharacterControl>();
    }

    //move towards;
    //check for attack possibility;
    //if in range of attack - attack;

    void Update()
    {
        Collider2D[] detectEnemies = Physics2D.OverlapCircleAll(characterControl.attackPoint.position, detectRange, enemyLayers);
        if(detectEnemies.Length > 0)
        {
            MoveToEnemy(detectEnemies[0].GetComponent<CharacterControl>().GetPos(), characterControl.transform.position);
        }
 
    }

    
    void MoveToEnemy(Vector3 enemyPos, Vector3 myPos)
    {
        characterControl.moveLeft = false;
        characterControl.moveRight = false;

        if (enemyPos.x > myPos.x && enemyPos.x < myPos.x + attackDistance || enemyPos.x < myPos.x && enemyPos.x > myPos.x - attackDistance)
        {
            if(Time.time >= nextAttack)
            {
                characterControl.moveLeft = false;
                characterControl.moveRight = false;
                Collider2D[] detectEnemies = Physics2D.OverlapCircleAll(characterControl.attackPoint.position, attackRange, enemyLayers);
                if (detectEnemies.Length > 0)
                {
                    characterControl.Attack = true;
                    nextAttack = Time.time + 1f / attackRate;
                }
            }

            return;
        }

        if(myPos.x < enemyPos.x)
        {
            characterControl.moveRight = true;
        }
        if(myPos.x > enemyPos.x)
        {
            characterControl.moveLeft = true;
        }
      
    }

}
