﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

namespace platformer
{
    public class ManualInput : MonoBehaviour
    {
        private CharacterControl characterControl;

        void Awake()
        {
            characterControl = this.gameObject.GetComponent<CharacterControl>();
        }

        void Update()
        {
            if(VirtualInputManager.Instance.moveLeft)
                characterControl.moveLeft = true;
            else
                characterControl.moveLeft = false;
            if (VirtualInputManager.Instance.moveRight)
                characterControl.moveRight = true;
            else
                characterControl.moveRight = false;

            if(VirtualInputManager.Instance.Jump && characterControl.RIGID_BODY.velocity.y < 0.01f && characterControl.RIGID_BODY.velocity.y > -0.01f)
            {
                characterControl.Jump = true;
            }
            else
                characterControl.Jump = false;
           
            if (VirtualInputManager.Instance.Attack)
                characterControl.Attack = true;
            else
                characterControl.Attack = false;
        }
    }

}
