﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace platformer
{

    [CreateAssetMenu(fileName = "New State", menuName = "platformer/AbilityData/Jump")]
    public class Jump : StateData
    {

        public float JumpForce;
        public override void OnEnter(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {
            characterState.GetCharacterControl(animator).RIGID_BODY.AddForce(Vector2.up * JumpForce, ForceMode2D.Impulse);
        }

        public override void UpdateAbility(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {
            CharacterControl control = characterState.GetCharacterControl(animator);
             if (control.RIGID_BODY.velocity.y <= 0f)
            {
                animator.SetBool(TransitionParameter.Jump.ToString(), false);
                
            }

        }
        public override void OnExit(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {
        }
    }

}
