﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace platformer
{
    [CreateAssetMenu(fileName = "New State", menuName = "platformer/AbilityData/Attack")]
    public class Attack : StateData
    {
        public float attackRange = 1f;
        public int attackDamage = 1;
        public LayerMask enemyLayers;
        public override void OnEnter(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {
            CharacterControl control = characterState.GetCharacterControl(animator);
            Collider2D[] hitenemies = Physics2D.OverlapCircleAll(control.attackPoint.position, attackRange, enemyLayers);
            foreach (Collider2D enemy in hitenemies)
            {
                enemy.GetComponent<Animator>().SetTrigger(TransitionParameter.Hit.ToString());
                enemy.GetComponent<CharacterControl>().TakeDamage(attackDamage);
            }

        }
        public override void UpdateAbility(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {
        }



        public override void OnExit(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {
            CharacterControl control = characterState.GetCharacterControl(animator);
            control.Attack = false;
        }
    }
}