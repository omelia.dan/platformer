﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace platformer
{

    [CreateAssetMenu(fileName = "New State", menuName = "platformer/AbilityData/Landing")]
    public class Landing : StateData
    {

        public override void OnEnter(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {
        }
        public override void UpdateAbility(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {
            CharacterControl control = characterState.GetCharacterControl(animator);
            if(control.RIGID_BODY.velocity.y <= 0f)
            {
                animator.SetBool(TransitionParameter.Grounded.ToString(), true);
            }


        }

        public override void OnExit(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {

        }

    }

}

