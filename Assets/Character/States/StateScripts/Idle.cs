﻿using platformer;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace platformer
{
    [CreateAssetMenu(fileName = "New State", menuName = "platformer/AbilityData/Idle")]
    public class Idle : StateData
    {
        public float speed;

        public override void OnEnter(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {
            animator.SetBool(TransitionParameter.Jump.ToString(), false);
        }
        public override void UpdateAbility(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {

            CharacterControl control = characterState.GetCharacterControl(animator);
            if(control.Attack)
            {
                animator.SetTrigger(TransitionParameter.Attack.ToString());
            }
            if (control.moveRight)
            {
                animator.SetBool(TransitionParameter.Move.ToString(), true);
            }
            if (control.moveLeft)
            {
                animator.SetBool(TransitionParameter.Move.ToString(), true);
            }
            if(control.Jump)
            {
                animator.SetBool(TransitionParameter.Jump.ToString(), true);
            }
        }

        public override void OnExit(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {
        }
    }
}

