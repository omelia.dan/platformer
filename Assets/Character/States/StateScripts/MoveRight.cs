﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.Speech;

namespace platformer
{
    [CreateAssetMenu(fileName = "New State", menuName = "platformer/AbilityData/MoveRight")]
    public class MoveRight : StateData
    {
        public float speed;

        public override void OnEnter(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {
  
        }
        public override void UpdateAbility(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {
            CharacterControl control = characterState.GetCharacterControl(animator);

            if(control.Jump && control.RIGID_BODY.velocity.y < 0.01f)
            {
                animator.SetBool(TransitionParameter.Jump.ToString(), true);
            }
            if (control.moveLeft && control.moveRight)
            {
                animator.SetBool(TransitionParameter.Move.ToString(), false);
                return;
            }
            if (control.moveRight)
            {
                control.transform.Translate(Vector2.right * speed * Time.deltaTime);
                control.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
            }
            if (control.moveLeft)
            {
                control.transform.rotation = Quaternion.Euler(0f, 180f, 0f);
                control.transform.Translate(Vector2.right * speed * Time.deltaTime);
            }

            if (!control.moveLeft && !control.moveRight)
            {
                animator.SetBool(TransitionParameter.Move.ToString(), false);
            }

        }

        public override void OnExit(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {

        }
    }
}

