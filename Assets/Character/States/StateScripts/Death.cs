﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace platformer
{
    [CreateAssetMenu(fileName = "New State", menuName = "platformer/AbilityData/Death")]
    public class Death : StateData
    {
        public override void OnEnter(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {
            animator.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
            animator.GetComponent<Rigidbody2D>().gravityScale = 0;
            animator.GetComponent<Collider2D>().enabled = false;
            animator.GetComponent<CharacterControl>().enabled = false;
            

        }
        public override void UpdateAbility(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {
            
        }



        public override void OnExit(CharacterState characterState, Animator animator, AnimatorStateInfo stateInfo)
        {

        }
    }
}