﻿using platformer;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.PlayerLoop;

enum TransitionParameter
{
    Move,
    Jump,
    Grounded,
    Attack,
    Hit,
    Death,
}

public class CharacterControl : MonoBehaviour
{
    public Animator animator;
    public bool moveRight;
    public bool moveLeft;
    public bool Jump;
    public bool Attack;
    public Transform attackPoint;
    public Transform pos;
    public int MaxHealth = 4;
    private int currentHealth;
    public int CurrentHealth { get
        {
            return currentHealth;
        } }


    public Vector3 GetPos()
    {
        if (pos == null)
            pos = GetComponent<Transform>();
        Vector3 temp = pos.position;
        temp.z = -10f;
        pos.position = temp;
        return pos.position;
    }

    private Rigidbody2D rigid;
    public Rigidbody2D RIGID_BODY
    {
        get
        {
            if (rigid == null)
            {
                rigid = GetComponent<Rigidbody2D>();
            }
            return rigid;
        }
    }

    private void Start()
    {
        currentHealth = MaxHealth;
    }

   

    public void TakeDamage(int dmg)
    {
        currentHealth -= dmg;

        if (animator.name == "Character")
        {

        }

    }
}
