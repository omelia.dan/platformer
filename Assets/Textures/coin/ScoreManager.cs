﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;
    public Text t;
    int score = 0;
    int numberOfCoins;


    void Start()
    {
        if(instance == null)
        {
            instance = this;
        }

        GameObject[] thingyToFind = GameObject.FindGameObjectsWithTag("Coins");
        numberOfCoins = thingyToFind.Length;
        changeScore(0);
    }

    public void changeScore(int coinValue)
    {
        score += coinValue;
        t.text = "Collected: " + score.ToString() + "/" + numberOfCoins;
        if (score == numberOfCoins)
            t.text = "Done!";
    }
}
