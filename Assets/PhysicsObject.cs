﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsObject : MonoBehaviour
{
    protected float gravitymodifier = 1f;
    protected Vector2 velocity;
    protected Rigidbody2D rb2d;

    private void OnEnable()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        velocity += gravitymodifier * Physics2D.gravity * Time.deltaTime;

        Vector2 deltaPosition = velocity * Time.deltaTime;

        Vector2 Move = Vector2.up * deltaPosition.y;

        movement(Move);

    }


    void movement(Vector2 move)
    {
        rb2d.position = rb2d.position + move;
    }
}
